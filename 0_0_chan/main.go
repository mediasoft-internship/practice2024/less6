package main

import "fmt"

func main() {
	ch := make(chan int)
	go func() {
		ch <- 1 // Блокируется, пока значение не будет прочитано
		close(ch)
	}()
	val := <-ch
	fmt.Println(val)
}
