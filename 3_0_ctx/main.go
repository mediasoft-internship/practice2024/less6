package main

import "context"

func main() {
	ctx := context.Background()
	go otherFunc(ctx)
	otherFunc(ctx)
}

func otherFunc(ctx context.Context) {}
