package main

import "fmt"

func main() {
	ch := make(chan int, 1)
	ch <- 1 // Результат выполнения зависит от этой строки.

	select {
	case val := <-ch:
		fmt.Println("Прочитано", val)
	default:
		fmt.Println("Канал пуст")
	}
}
