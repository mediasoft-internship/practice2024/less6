package main

import (
	"context"
	"fmt"
	"time"
)

type Worker struct {
	name     string
	workCB   func(string)
	interval time.Duration
}

func NewWorker(name string, workCB func(string), interval time.Duration) *Worker {
	return &Worker{name: name, workCB: workCB, interval: interval}
}

func (w *Worker) Work(ctx context.Context, doneCh chan struct{}) {
	go func() {
		for {
			select {
			case <-ctx.Done():
				fmt.Printf("Воркер с именем %s завершил свою работу\n", w.name)
				doneCh <- struct{}{}
				return
			case <-time.After(w.interval):
				w.workCB(w.name)
			}
		}
	}()
}

func main() {
	ctx, stop := context.WithCancel(context.Background())

	reviews := make(chan string)

	workers := [...]*Worker{
		NewWorker(
			"Младший программист",
			func(name string) {
				fmt.Println(name + ": Написал код, передаю его на ревью")
				reviews <- "Плохой код"
			},
			time.Second*2,
		),
		NewWorker(
			"Старший программист",
			func(name string) {
				select {
				case review, reviewOk := <-reviews:
					if !reviewOk {
						return
					}

					fmt.Println(name + ": Пришёл код на ревью, надо глянуть")
					fmt.Printf("%s Глянул, а там '%s', надо переделать\n", name, review)
				default:
					fmt.Println(name + ": Нового кода пока нет, можно отдохнуть")
				}
			},
			time.Second,
		),
	}

	done := make(chan struct{})

	for _, worker := range workers {
		worker.Work(ctx, done)
	}

	time.Sleep(time.Second * 10)
	stop()
	for range workers {
		<-done
	}
}
