package main

import (
	"context"
	"fmt"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		<-ctx.Done()
		fmt.Println("Операция отменена")
	}()

	cancel() // Отменяем контекст и все связанные с ним операции
}
