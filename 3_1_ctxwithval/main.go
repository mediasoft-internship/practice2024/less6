package main

import (
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "key", "value")

	value := ctx.Value("key")
	fmt.Println(value) // Выведет "value"
}
