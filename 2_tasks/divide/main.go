package main

import (
	"fmt"
	"math/rand"
	"sync"
)

/*
Необходимо написать функцию, которая "разделяет" один канал на два
*/

func divide(ch <-chan string) (<-chan string, <-chan string) {
	aCh, bCh := make(chan string), make(chan string)

	go func() {
		defer close(aCh)
		defer close(bCh)

		for val := range ch {
			if rand.Intn(2) == 0 {
				aCh <- val
			} else {
				bCh <- val
			}
		}
	}()

	return aCh, bCh
}

func main() {
	ch := make(chan string)

	aCh, bCh := divide(ch)

	go func() {
		for i := 0; i < 10; i++ {
			ch <- fmt.Sprintf("ch-%d", i)
		}

		close(ch)
	}()

	wg := sync.WaitGroup{}

	wg.Add(2)

	go func() {
		for val := range aCh {
			fmt.Println("a: " + val)
		}
		wg.Done()
	}()

	go func() {
		for val := range bCh {
			fmt.Println("b: " + val)
		}
		wg.Done()
	}()

	wg.Wait()
}
