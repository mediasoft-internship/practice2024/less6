package main

import (
	"fmt"
	"time"
)

/*
Необходимо написать функцию, которая "объединяет" два канала в один
*/

func merge(a <-chan string, b <-chan string) <-chan string {
	result := make(chan string)

	go func() {
		defer close(result)

		var val string

		aOk, bOk := true, true

		for {
			select {
			case val, aOk = <-a:
				if !aOk {
					break
				}
				result <- val
			case val, bOk = <-b:
				if !bOk {
					break
				}
				result <- val
			}

			if !aOk || !bOk {
				break
			}
		}
		if aOk {
			for val := range a {
				result <- val
			}
			return
		}
		for val := range b {
			result <- val
		}
	}()

	return result
}

func main() {
	aCh, bCh := make(chan string), make(chan string)

	res := merge(aCh, bCh)

	go func() {
		for i := 0; i < 5; i++ {
			aCh <- fmt.Sprintf("a-%d", i)
			time.Sleep(time.Millisecond)
		}

		close(aCh)
	}()

	go func() {
		for i := 0; i < 3; i++ {
			bCh <- fmt.Sprintf("b-%d", i)
			time.Sleep(time.Millisecond)
		}

		close(bCh)
	}()

	for val := range res {
		fmt.Println(val)
	}
}
