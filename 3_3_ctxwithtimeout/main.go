package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	go func() {
		select {
		case <-time.After(3 * time.Second):
			fmt.Println("Операция выполнена")
		case <-ctx.Done():
			fmt.Println("Операция прервана по тайм-ауту")
		}
	}()
}
