package main

import "fmt"

func main() {
	ch := make(chan int)
	ch <- 1 // Блокируется, и поскольку никто не может прочитать, то ловим deadlock
	close(ch)
	val := <-ch
	fmt.Println(val)
}
